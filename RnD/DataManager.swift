//
//  DataManager.swift
//  RnD
//
//  Created by Fernando Zanei on 2018-11-15.
//  Copyright © 2018 fernandozanei. All rights reserved.
//

import Foundation
import CoreLocation

class DataManager {
    var citiesSet: Set<City>
    var cities: [City]
    
    init() {
        citiesSet = Set<City>()
        cities = [City]()
    }
    
    func loadData(completion: @escaping () -> Void) {
        if let path = Bundle.main.path(forResource: "cities", ofType: "json") {
            do {
                let jsonData = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let json = try JSONSerialization.jsonObject(with: jsonData) as! [[String: Any]]
                
                for city in json {
                    if let country = city["country"] as? String,
                        let name = city["name"] as? String,
                        let id = city["_id"] as? Int,
                        let coord = city["coord"] as? [String:Any],
                        let lat = coord["lat"] as? Double,
                        let lon = coord["lon"] as? Double {
                        citiesSet.insert(City(country: country,
                                              name: name,
                                              id: id,
                                              location: CLLocation(latitude: lat, longitude: lon)))
                    }
                }
                
                cities = Array(citiesSet.sorted  {
                    if $0.name != $1.name {
                        return $0.name < $1.name
                    } else {
                        return $0.country < $1.country
                    }
                })
            } catch {
                // error - Handling error can (in this case we have the file, so will never enter here)
            }
        }
        completion()
    }
}
