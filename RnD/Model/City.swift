//
//  City.swift
//  RnD
//
//  Created by Fernando Zanei on 2018-11-15.
//  Copyright © 2018 fernandozanei. All rights reserved.
//

import Foundation
import CoreLocation

class City: Equatable, Hashable {
    
    let country: String
    let name: String
    let id: Int
    let location: CLLocation
    
    init(country: String, name: String, id: Int, location: CLLocation) {
        self.country = country
        self.name = name
        self.id = id
        self.location = location
    }

    // Conforming to be able to use with a Set
    var hashValue: Int {
        get {
            return name.hashValue
        }
    }
    static func == (lhs: City, rhs: City) -> Bool {
        return lhs.name == rhs.name && lhs.country == rhs.country
    }
}
