//
//  ViewController.swift
//  RnD
//
//  Created by Fernando Zanei on 2018-11-15.
//  Copyright © 2018 fernandozanei. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    let data = DataManager()
    var cities = [City]()
    var filteredCities: [City]!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        data.loadData {
            self.cities = self.data.cities
            self.filteredCities = self.cities
            self.tableView.reloadData()
        }
    }
    
    // MARK: - Table view

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredCities.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)// as! UITableViewCell
        cell.textLabel?.text = "\(filteredCities[indexPath.row].name), \(filteredCities[indexPath.row].country)"
        return cell
    }
    
    // MARK: - Search Bar

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        filteredCities = searchText.isEmpty ? cities : cities.filter({(city: City) -> Bool in
            return city.name.range(of: searchText, options: [.caseInsensitive, .anchored]) != nil
        })
        
        tableView.reloadData()
    }
    
    // MARK: - Segue
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinationVC = segue.destination as? MapViewController {
            let indexPath = tableView.indexPathForSelectedRow!
            destinationVC.city = filteredCities[indexPath.row]
            destinationVC.navigationItem.title = "\(filteredCities[indexPath.row].name), \(filteredCities[indexPath.row].country)"
        }
    }

}

