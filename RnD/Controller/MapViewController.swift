//
//  MapViewController.swift
//  RnD
//
//  Created by Fernando Zanei on 2018-11-16.
//  Copyright © 2018 fernandozanei. All rights reserved.
//

import UIKit
import MapKit

class MapViewController: UIViewController {

    @IBOutlet weak var mapView: MKMapView!
    var city: City!
    // radius to set initial zoom on map
    let regionRadius: CLLocationDistance = 10000

    override func viewDidLoad() {
        super.viewDidLoad()
        centerMapOnLocation(location: city.location)
    }

    func centerMapOnLocation(location: CLLocation) {
        let coordinateRegion = MKCoordinateRegion(center: city.location.coordinate,
                                                  latitudinalMeters: regionRadius, longitudinalMeters: regionRadius)
        mapView.setRegion(coordinateRegion, animated: true)
    }

}
