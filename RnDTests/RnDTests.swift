//
//  RnDTests.swift
//  RnDTests
//
//  Created by Fernando Zanei on 2018-11-15.
//  Copyright © 2018 fernandozanei. All rights reserved.
//

import XCTest
import CoreLocation
@testable import RnD

class RnDTests: XCTestCase {
    
    var systemUnderTest: ViewController!
    var searchText = ""
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        systemUnderTest = storyboard.instantiateViewController(withIdentifier: "tableview") as? ViewController
        _ = systemUnderTest.view
        
        let cities = [
            City(country: "US", name: "Alabama", id: 0, location: CLLocation(latitude: 0.0, longitude: 0.0)),
            City(country: "US", name: "Albuquerque", id: 0, location: CLLocation(latitude: 0.0, longitude: 0.0)),
            City(country: "US", name: "Anaheim", id: 0, location: CLLocation(latitude: 0.0, longitude: 0.0)),
            City(country: "US", name: "Arizona", id: 0, location: CLLocation(latitude: 0.0, longitude: 0.0)),
            City(country: "AU", name: "Sydney", id: 0, location: CLLocation(latitude: 0.0, longitude: 0.0))
        ]
        
        systemUnderTest.cities = cities
        systemUnderTest.filteredCities = cities
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func test_searchPassingA() {
        searchText = "A"
        systemUnderTest.searchBar(systemUnderTest.searchBar, textDidChange: searchText)
        
        let citiesWithA = ["Alabama, US", "Albuquerque, US", "Anaheim, US", "Arizona, US"]
        let resultCities = systemUnderTest.filteredCities.map {
            return "\($0.name), \($0.country)"
        }
        
        XCTAssertEqual(resultCities, citiesWithA)
    }
    
    func test_searchPassingAl() {
        searchText = "Al"
        systemUnderTest.searchBar(systemUnderTest.searchBar, textDidChange: searchText)
        
        let citiesWithAl = ["Alabama, US", "Albuquerque, US"]
        let resultCities = systemUnderTest.filteredCities.map {
            return "\($0.name), \($0.country)"
        }
        
        XCTAssertEqual(resultCities, citiesWithAl)
    }

    func test_searchPassingAlb() {
        searchText = "Alb"
        systemUnderTest.searchBar(systemUnderTest.searchBar, textDidChange: searchText)
        
        let citiesWithAlb = ["Albuquerque, US"]
        let resultCities = systemUnderTest.filteredCities.map {
            return "\($0.name), \($0.country)"
        }
        
        XCTAssertEqual(resultCities, citiesWithAlb)
    }

    func test_searchPassingS() {
        searchText = "S"
        systemUnderTest.searchBar(systemUnderTest.searchBar, textDidChange: searchText)
        
        let citiesWithS = ["Sydney, AU"]
        let resultCities = systemUnderTest.filteredCities.map {
            return "\($0.name), \($0.country)"
        }
        
        XCTAssertEqual(resultCities, citiesWithS)
    }

    func test_searchPassingZ() {
        searchText = "Z"
        systemUnderTest.searchBar(systemUnderTest.searchBar, textDidChange: searchText)
        
        XCTAssertTrue(systemUnderTest.filteredCities.count == 0)
    }

    func test_searchPassingInvalidChar() {
        searchText = "#"
        systemUnderTest.searchBar(systemUnderTest.searchBar, textDidChange: searchText)
                
        XCTAssertTrue(systemUnderTest.filteredCities.count == 0)
    }
}
